# Music Engraving Conference 2020, Music University Mozarteum, Salzburg

All files found in this repository are under the BY-NC-ND 4.0 Creative
Commons License (except stated otherwise).

  https://creativecommons.org/licenses/by-nc-nd/4.0/
